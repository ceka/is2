#!/bin/bash


echo -e "Generando Documentacion de SKP [GENERANDO]..."
echo -e "Borrando la base de datos SKP antigua."
# echo -e "Ingrese la contrasenha del usuario skp."
dropdb -i --if-exists -U skp skp
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos SKP, verifique que nadie la este usando."
    exit 1
fi
echo -e "Se ha borrado SKP antigua."

echo -e "Creando la base de datos SKP."
echo -e "Ingrese la contrasenha del usuario skp."
createdb -U skp skp
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear la base de datos SKP."
    exit 2
fi
echo -e "Se ha creado SKP."

echo -e "Restaurando el SCHEMA de la Base de Datos desde el archivo db_schema.sql"
psql -U skp -d skp  -f db_schema.sql

if [ "$?" -ne 0 ]
then
    echo -e "Ocurrio un problema al intentar crear la estructura de la BD."
    exit 3
fi
echo -e "SCHEMA de SKP creada."

echo -n "Para Restaurar los DATOS desde el archivo db_data.sql, presione [ENTER]: "
read name
psql -U skp -d skp  -f db_data.sql

if [ "$?" -ne 0 ]
then
    echo -e "Ocurrio un problema al cargar los DATOS a la BD."
    exit 4
fi
echo -e "DATOS cargados a la DB SKP exitosamente."

exit 0
