__author__ = 'miguel'

from django.forms import ModelForm
from apps.user_stories.models import User_Story
from django import forms
from django.utils.datetime_safe import datetime
today = datetime.now()                  #fecha actual
dateFormat = today.strftime("%Y-%m-%d") # fecha con formato


class IniciarSprintForm(forms.Form):
    """
    Formulario para la seleccion de los User Stories que van en el Sprint que se va a iniciar
    Los campos de Fecha de inicio y Fecha de Finalizacion son cargados automaticamente por el sistema

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Miguel Pereira
    """

    #solo listo los que no estan finalizados
    user_story = forms.ModelMultipleChoiceField(queryset=User_Story.objects.filter(finalizado=False),
                                                widget=forms.CheckboxSelectMultiple())

    def __init__(self, proyecto, *args, **kwargs):
        super(IniciarSprintForm, self).__init__(*args, **kwargs)

        self.fields['user_story'].queryset = User_Story.objects.filter(finalizado=False, borrado_logico=False,proyecto = proyecto).order_by('-prioridad')
