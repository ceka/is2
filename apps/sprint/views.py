import json

__author__ = 'miguel'

import datetime
#from django.utils.datetime_safe import datetime
from dateutil.relativedelta import relativedelta

from django.shortcuts import render_to_response, render
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from apps.proyecto.models import Proyecto
from apps.user_stories.models import User_Story
from apps.flujos.models import Flujo, Actividad
from apps.sprint.models import Sprint
from apps.sprint.forms import IniciarSprintForm
from apps.user_stories.models import Comentario

@login_required(login_url='/login/')
def iniciarSprint(request, id_proyecto):
    """
    Se recibe un request y el id del proyecto por al cual se accedio a la vista, y crea un nuevo sprint
    con la fecha actual y calcula la fecha de fin. Muestra la lista de US disponibles para agregar, Filtrando
    los mismos los que no estan finalizados y no estan borrados.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: sprint.html, donde se se ve el sprint actual, y el boton de iniciar nuevo sprint

    :author: Miguel Pereira
    """

    proyecto = Proyecto.objects.get(id=id_proyecto)

    hoy = datetime.datetime.now()  # fecha actual
    dateFormat = hoy.strftime("%Y-%m-%d")  # fecha con formato XXXX SE VA A CAMBIAR CUANDO EDU SUBA SUS CAMBIOS

    # calculo de la fecha de finalizacion del sprint
    fecha_despues = datetime.datetime.today() + relativedelta(days=proyecto.duracion_sprint)  # dentro de 1 mes despues termina
    fecha_final = fecha_despues.strftime('%Y-%m-%d')

    # lista de todos los sprints asociados al proyecto, ordenado descendente mente, el mayor esta primero
    # para que NO tire error en caso que lista_sprint este vacia
    try:
        ultimo_sprint = Sprint.objects.filter(proyecto=id_proyecto).order_by('-numero')[0]
    except :
        ultimo_sprint = None

    if ultimo_sprint is None:               # caso trivial del primer sprint
        nro_sprint = 1
    # de la manera anterior me aseguro que nunca haya mas de 1 sprint corriendo, asi me ahorro la
    # verificacion tambien en la vista verSprint.
    elif ultimo_sprint.estado == 'FIN':          # el ultimo sprint ya termino
        nro_sprint = ultimo_sprint.numero + 1  # agarro el ultimo, y mayor de todos a la vez, y le sumo 1
    else:                      #hay otro sprint con estado Activo o TRUE, por lo que no se puede iniciar otro
        form = IniciarSprintForm(proyecto)
        template_name = './sprint/iniciar_sprint.html'
        return render(request, template_name, {'form': form, 'mensaje' : 'No se puede iniciar un Sprint estando otro Activo'})


    if request.method == 'POST':
        form = IniciarSprintForm(proyecto,request.POST)
        if form.is_valid():
            form.clean()
            user_story = form.cleaned_data['user_story']        # traigo los user_story seleccionados en el form

            nuevo_sprint = Sprint.objects.create(numero=nro_sprint, proyecto=proyecto, estado='ACT', fecha_ini=dateFormat, fecha_fin=fecha_final) #user_story=us,
            nuevo_sprint.save()
            for us in user_story:
                nuevo_sprint = Sprint.objects.get(numero=nro_sprint, proyecto=id_proyecto)
                nuevo_sprint.user_story.add(us.id)
                nuevo_sprint.save()
                us.tiene_sprint = True
                us.save()

            template_name = './sprint/sprint_iniciado.html'
            return render(request, template_name, {'id_proyecto': id_proyecto})

    else:
        form = IniciarSprintForm(proyecto)

    template_name = './sprint/iniciar_sprint.html'
    return render(request, template_name, {'form': form})

@login_required(login_url='/login/')
def verSprint(request, id_proyecto, numero_sprint):
    """
    Se recibe un request, el id del proyecto y el id del sprint por para mostrar los US del sprint,

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: sprint.html, donde se se ve el sprint actual, y el boton de iniciar nuevo sprint

    :author: Miguel Pereira
    """
    usuario_actor = request.user

    sprint_actual = Sprint.objects.get(numero=numero_sprint, proyecto=id_proyecto)
    lista = sprint_actual.user_story.all().order_by('id')
    lista_flujo = Flujo.objects.filter(proyecto=id_proyecto)


    ctx = {'usuario_actor': usuario_actor,
           'lista_us': lista,
           'id_proyecto': id_proyecto,
           'numero_sprint': numero_sprint,
           'lista_flujo': lista_flujo,
           }
    return render_to_response('sprint/detalle_sprint.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def adminisrarSprint(request, id_proyecto):
    """
    Se recibe un request y el id del proyecto por al cual se accedio a la vista, y se muestra la lista de
    todos los Sprints que hay para el proyecto. Tambien se muestra el boton para iniciar un nuevo Sprint, si no
    hay un sprint activo.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: sprint.html, donde se se ve el sprint actual, y el boton de iniciar nuevo sprint

    :author: Miguel Pereira
    """

    usuario_actor = request.user
    permite_iniciar_sprint = True       # por defecto sale el boton de iniciar sprint en el html
    texto_faltan = ''
    porcentaje = '0'  # inicializo en cero, por si no haya sprint activo, este valor se muestra como barra
    texto = ' No hay Sprint Activo'

    # para el calculo del porcentaje del Sprint Activo
    try:
        ultimo_sprint = Sprint.objects.filter(proyecto=id_proyecto).order_by('-numero')[0]
    except :
        ultimo_sprint = None

    # para que no explote en el caso trivial de cuando no hay ningun sprint aun
    if ultimo_sprint is not None:
        fin = datetime.datetime.strptime(str(ultimo_sprint.fecha_fin), "%Y-%m-%d")
        inicio = datetime.datetime.strptime(str(ultimo_sprint.fecha_ini), "%Y-%m-%d")
        duracion_sprint = abs((inicio - fin).days)

        hoy = datetime.datetime.now()  # fecha actual
        hoyFormat = hoy.strftime("%Y-%m-%d")

        d3 = datetime.datetime.strptime(str(hoyFormat), "%Y-%m-%d")
        d4 = datetime.datetime.strptime(str(ultimo_sprint.fecha_ini), "%Y-%m-%d")
        hecho = abs((d3 - d4).days)
        faltan = abs((fin - d3).days)


        if ultimo_sprint.estado == 'ACT':
            if d3 > fin: # quiere decir que ya termino
                ultimo_sprint.estado = 'FIN'
                ultimo_sprint.save()
            else:
                porcentaje = int((float(hecho) / float(duracion_sprint)) * 100)
                texto = ''
                texto_faltan = ' Quedan ' + str(faltan) + ' dias.'
                permite_iniciar_sprint = False  # hay un sprint activo, no se muestra el boton de iniciar sprint


    # para que NO tire error en caso que la lista este vacia
    try:
        sprints = Sprint.objects.filter(proyecto=id_proyecto).order_by('-numero') # ordenado descendente
    except :
        sprints = 0

    ctx = {'usuario_actor': usuario_actor,
           'lista': sprints,
           'id_proyecto': id_proyecto,
           'porcentaje': porcentaje,
           'texto': texto,
           'texto_faltan': texto_faltan,
           'permiso': permite_iniciar_sprint
    }
    return render_to_response('sprint/sprint.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def verKanban(request, id_proyecto, numero_sprint, numero_flujo):
    """
    Se recibe un request y el numero del Sprint, y se muestra su tablero del Kanban.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: kanban.html, donde se se ve el kanban del sprint actual, y el boton de volver a sprint.

    :author: Miguel Pereira
    """

    usuario_actor = request.user

    sprint = Sprint.objects.get(numero=numero_sprint, proyecto=id_proyecto)       # traigo el sprint completo

    # todos los flujos del proyecto
    flujo = Flujo.objects.get(id=numero_flujo)

    # mando completa la lista de actividades en la base de datos, dentro del html itero de acuerdo
    # al flujo
    lista_actividades = Actividad.objects.all()

    lista_us = sprint.user_story.filter(flujo=numero_flujo)
    lista_estados = ['TODO', 'DOING', 'DONE']

    ctx = {'usuario_actor': usuario_actor,
           'lista_us': lista_us,
           'lista_estados': lista_estados,
           'lista_actividades' : lista_actividades,
           'flujo': flujo,
           'id_proyecto' : id_proyecto,
           'numero_sprint' : numero_sprint
           }
    return render_to_response('sprint/kanban.html', ctx, context_instance=RequestContext(request))

def productBacklog(request, id_proyecto):
    """
    Se recibe un request, el id del proyecto, para mostrar el Producto Backlog del Proyecto
    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: product_backlog.html, donde se sen los User Stories

    :author: Miguel Pereira
    """
    usuario_actor = request.user

    try:
        ultimo_sprint = Sprint.objects.filter(proyecto=id_proyecto).order_by('-numero')[0]
    except :
        ultimo_sprint = None

    # para que no explote en el caso trivial de cuando no hay ningun sprint aun
    if ultimo_sprint is None:
        lista_us = User_Story.objects.filter(proyecto= id_proyecto, borrado_logico= False).order_by('id')
    elif ultimo_sprint.estado == 'FIN':
        lista_us = User_Story.objects.filter(proyecto= id_proyecto, borrado_logico= False).order_by('id')
    else:
        lista_us = User_Story.objects.filter(proyecto= id_proyecto, borrado_logico= False, tiene_sprint= False).order_by('id')

    ctx = {'usuario_actor': usuario_actor,
           'lista_us': lista_us,
           'id_proyecto': id_proyecto,
           }
    return render_to_response('sprint/product_backlog.html', ctx, context_instance=RequestContext(request))

def generarBurnDownChart(request, id_proyecto, numero_sprint):
    proyecto = Proyecto.objects.get(id=id_proyecto)
    sprint = Sprint.objects.get(numero=numero_sprint, proyecto=proyecto)       # traigo el sprint completo
    today = datetime.datetime.now()

    comentarios = list()

    duracionSprint = proyecto.duracion_sprint
    fecha = sprint.fecha_ini
    listaHU = User_Story.objects.filter(tiene_sprint=True)
    max = 0
    horas = 0


    for hu in listaHU:
        concatenado = list(Comentario.objects.filter(user_story=hu))
        comentarios = comentarios + concatenado
        max = max + hu.size
        #print(comentarios[0].fecha.day)
#En teoria, en la lista comentarios, estan TODOS LOS COMENTARIOS DEL SPRINT


    a = proyecto.duracion_sprint
    listaEnviar = list(((0,max),))

    for i in range(duracionSprint):

        for comentario in comentarios:
            if(comentario.fecha.day == fecha.day and comentario.fecha.year == fecha.year and comentario.fecha.month == fecha.month):
#Hasta aca, estoy preguntando si de la lista de comentarios, el comentario en donde estoy, es de la fecha del sprint
                horas= horas + comentario.horas_dedicadas

        if fecha.day > today.day:
            break
        fecha = fecha + relativedelta(days=1)#Aca estoy sumando la fecha mas 1

        listaEnviar = listaEnviar+ list(((i+1,max-horas),))#Aca le sumo la tupla a la lista que se le va a pasar al BDC

        listaIdeal = ((0,max),(proyecto.duracion_sprint,0))
        print(listaIdeal)
        return render_to_response('sprint/bdc.html',{'id_proyecto':id_proyecto , 'numero_sprint':numero_sprint , 'calculado':json.dumps(listaEnviar),'ideal':json.dumps(listaIdeal)})