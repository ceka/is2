__author__ = 'miguel'

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from apps.user_stories.models import User_Story
from apps.proyecto.models import Proyecto
from apps.sprint.models import Sprint
from apps.sprint.views import iniciarSprint


class test_sprint(TestCase):
    """
    Para realizar todos los tests a las funciones de vistas de sprint

    :author: Miguel Pereira
    """
    #cargar los datos
    fixtures = ['db_dump.json']

    def setUp(self):
        """ Inicializamos la variable factory que posteriormente nos permitira cargar
            un request para utilizarlo en las vista.
        """
        self.factory = RequestFactory()

    def test_iniciarSprint(self):
        """
        Crea una instancia de Sprint, mediante una solicitud GET
        """

        request = self.factory.get('/adm_proyectos/1/sprint/')

        #Asignamos a la variable user el usuario admin, y es este el que realiza las solicitudes.
        self.user = User.objects.get(pk=1)
        request.user = self.user

        # verifico que el usuario admin existe
        existeUsuario = User.objects.filter(pk=1)
        self.assertTrue(existeUsuario)

        # busco un proyecto
        proyecto = Proyecto.objects.get(id=1)
        self.assertTrue(proyecto)

        # busco un US cualquiera
        user_story = User_Story.objects.get(id=2)
        self.assertTrue(user_story)

        # imprimo todos los sprints antes de crear el nuevo
        print Sprint.objects.all()

        #voy a la pagina de creacion de cliente con su formulario
        request = self.factory.post('/adm_proyectos/1/sprint/iniciar_sprint/', {'user_story': user_story.id})
        request.user = self.user

        #verifico que este en la pagina de "usuario creado con exito"
        respuesta = iniciarSprint(request, proyecto.id)
        self.assertTrue(respuesta.status_code, 200)

        # imprimo todos los sprints para verificar que se haya creado
        print Sprint.objects.all()

        print 'Test de iniciar SPRINT ejecutado exitosamente.'