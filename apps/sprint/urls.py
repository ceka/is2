__author__ = 'miguel'

from django.conf.urls import patterns, url
from .views import iniciarSprint, verSprint, adminisrarSprint, verKanban, productBacklog,generarBurnDownChart

urlpatterns = patterns('',
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/iniciar_sprint/$', iniciarSprint),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/$', adminisrarSprint),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/ver/(?P<numero_sprint>\d+)/$', verSprint),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/ver/(?P<numero_sprint>\d+)/kanban/(?P<numero_flujo>\d+)/$', verKanban),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/product_backlog/$', productBacklog),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/sprint/ver/(?P<numero_sprint>\d+)/burn_down_chart/$', generarBurnDownChart),



        )