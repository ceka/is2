from django.contrib.auth.models import User
from django.db.models import Q
from apps.flujos.models import Actividad
from apps.proyecto.models import Proyecto, Equipo
from django.shortcuts import render_to_response, render
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from apps.user_stories.forms import UserStoryNuevoForm, UserStoryModificadoForm,asignarFlujoForm,actualizarProgresoForm
from apps.user_stories.models import User_Story,Comentario,Historial
from django.utils.datetime_safe import datetime
from django.core.mail import send_mail


__author__ = 'eduardo'


@login_required(login_url='/login/')
def administrarUserStory(request,id_proyecto):
    """
    Se recibe un request y el id del proyecto por el cual se accedio a la vista, y se listan los HU creados
    que corresponden a ese proyecto, para luego pasar al HTML que muestra la vista.
    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: user_story.html, donde se listan los flujos, ademas de las funcionalidades para los flujos

    :author: Eduardo Mendez
    """
    usuario_actor = request.user
    lista_hu = User_Story.objects.filter(proyecto= id_proyecto, borrado_logico= False).order_by('-prioridad') #trae todos, hasta los inactivos

    ctx = {'usuario_actor':usuario_actor,'lista_hu':lista_hu, 'id_proyecto':id_proyecto}
    return render_to_response('UserStory/user_story.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def crearUserStory(request,id_proyecto):
    """
    Recibe un request y el id del proyecto por el cual se accedio, para luego mostrar el formulario de creacion
    de un user story nuevo, verifica la integridad de los datos cargados, crea y guarda el user story nuevo.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: user_story_creado.html, donde se muestra un mensaje de creacion exitosa

    :author: Eduardo Mendez
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")


    if request.method == 'POST':
        form = UserStoryNuevoForm(proyecto,request.POST)
        if form.is_valid():
            form.clean()
            nombre = form.cleaned_data['Nombre']
            descripcion= form.cleaned_data['Descripcion']
            prioridad = form.cleaned_data['Prioridad']
            valNegocio = form.cleaned_data['Valor_De_Negocio']
            valTecnico = form.cleaned_data['Valor_Tecnico']
            size = form.cleaned_data['Size']
            size = size*6

            miembro = form.cleaned_data['Usuario_Asociado']

            flujo = form.cleaned_data['Flujo']
            try:
                actividad = Actividad.objects.filter(flujo=flujo).order_by('orden')[0]
            except:
                actividad = None

            try:
                usExistente = User_Story.objects.get(nombre=nombre, proyecto=proyecto,borrado_logico= False)
            except:
                usExistente = None

            if actividad is None:
                form = UserStoryNuevoForm(proyecto,request.POST)
                template_name = './UserStory/crear_user_story.html'
                return render(request, template_name, {'form': form, 'mensaje' : 'No se puede crear el User Story porque el flujo seleccionado no tiene actividades registradas.', 'id_proyecto':id_proyecto})

            if usExistente:
                form = UserStoryNuevoForm(proyecto,request.POST)
                template_name = './UserStory/crear_user_story.html'
                return render(request, template_name, {'form': form, 'mensaje' : 'User Story Duplicado dentro del Proyecto.', 'id_proyecto':id_proyecto})

            estado = 'TODO'
            usuario_modificacion = request.user


            userStoryNuevo = User_Story(nombre=nombre, descripcion=descripcion,prioridad=prioridad, valor_negocio=valNegocio,
                                        valor_tecnico=valTecnico, size=size, horas_dedicadas= 0,proyecto=proyecto,
                                        flujo=flujo,actividad = actividad,estado=estado, usuario_asociado=miembro,
                                        usuario_modificacion= usuario_modificacion, fecha_modificacion = today)

            historico = "User Story creado por "+str(usuario_modificacion)+" en fecha "+str(today)

            userStoryNuevo.actividad = actividad
            userStoryNuevo.save()
            historialNuevo = Historial(descripcion=historico,user_story=userStoryNuevo)
            historialNuevo.save()
            send_mail("Asunto",
            historico,
            '"Administrador del Sistema"',
            [proyecto.lider.email])
            template_name = './UserStory/user_story_creado.html'
            return render(request, template_name, {'id_proyecto': id_proyecto})


    else:


        form = UserStoryNuevoForm(proyecto)

    template_name = './UserStory/crear_user_story.html'
    return render(request, template_name, {'form': form, 'id_proyecto':id_proyecto})

@login_required(login_url='/login/')
def asignarFlujo(request,id_proyecto,id_user_story):
    """
    Vista para cambiar del flujo a un HU siempre que este no se encuentre dentro de un sprint.
    :return render_to_response:
    :return render:
    :param request:
    :param id_user_story: id del user story al que se quiere cambiar de flujo
    """
    if puede_realizar_accion(request.user,id_proyecto,'asignar_flujo'):
        proyecto = Proyecto.objects.get(id=id_proyecto)
        usModificado = User_Story.objects.get(id= id_user_story)

        if request.method == 'POST':
            form = asignarFlujoForm(proyecto,request.POST)
            if form.is_valid():
                form.clean()

                flujo = form.cleaned_data['Flujo_Actual'] #variable que vamos a usar para comparar el nuevo flujo con el viejo
                ultimaActividad = Actividad.objects.filter(flujo_id= usModificado.flujo.id).order_by('-orden')[0] #tengo que saber si del flujo en el que esta actualmente,se llego a la ultima actividad
                if(usModificado.flujo != flujo ): #si el nuevo flujo es diferente al viejo, hay que verificar que se llego a la ultima actividad del viejo flujo
                    if(usModificado.actividad != ultimaActividad):# se pregunta si la actividad del US, es diferente a la ultima actividad del flujo del US
                        template_name = './UserStory/asignar_flujo_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puede cambiar de Flujo ya que no ha alcanzado la ultima actividad del Flujo actual',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    elif(usModificado.estado !='DONE'):

                        template_name = './UserStory/asignar_flujo_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puede cambiar de Flujo ya que no ha alcanzado el ultimo estado de la ultima actividad del flujo actual',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    try:
                        actividad = Actividad.objects.filter(flujo=flujo).order_by('orden')[0]
                    except:
                        actividad = None

                    if actividad is None:
                        template_name = './UserStory/asignar_flujo_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No se puede crear el User Story porque el flujo seleccionado no tiene actividades registradas.',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    else:
                        usModificado.flujo= flujo
                        usModificado.actividad = Actividad.objects.filter(flujo=flujo).order_by('orden')[0]
                        usModificado.estado = 'TODO'

                usModificado.save()

                template_name = './UserStory/flujo_asignado.html'
                return render(request, template_name, {'id_proyecto': id_proyecto})


        else:
            data={'Flujo_Actual':usModificado.flujo.id}
            form = asignarFlujoForm(proyecto,data)

        template_name = './UserStory/asignar_flujo_user_story.html'
        return render(request, template_name, {'form': form,'id_proyecto':id_proyecto})
    else:
        template_name = '403_flujos_us.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def finalizarUserStory(request,id_proyecto,id_user_story):
    proyecto = Proyecto.objects.get(id=id_proyecto)
    us = User_Story.objects.get(id = id_user_story)
    listaComentario = Comentario.objects.filter(user_story = us).order_by("fecha")
    sizeEnDias= int(float(us.size)/6.0)

    ultimaActividad = Actividad.objects.filter(flujo_id= us.flujo.id).order_by('-orden')[0] #tengo que saber si del flujo en el que esta actualmente,se llego a la ultima actividad

    if(us.actividad != ultimaActividad and us.estado != 'DONE'):# se pregunta si la actividad del US, es diferente a la ultima actividad del flujo del US
        template_name = './UserStory/ver_user_story.html'
        return render(request, template_name, {'mensaje': 'No se puede validar el User Story debido a que todavia no culmino su flujo respectivo.',
                                               'us':us,
                                               'id_proyecto': id_proyecto,
                                                'comentarios':listaComentario,
                                                'size':sizeEnDias})
    else:
        us.finalizado = True
        us.save()
        template_name = './UserStory/ver_user_story.html'
        return render(request, template_name, {'us':us,
                                               'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def verUserStory(request, id_user_story,id_proyecto):
    """
    Recibe un request, el id del User Story que se desea visualizar y el id del proyecto por el cual se accedio,
    para luego mostrar el formulario de visualizacion de un User Story.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_user_story : integer.
    :param id_user_story : Contiene el id del flujo a ser visualizado.

    :rtype: django.http.HttpResponse
    :return: ver_flujo.html, donde se muestran los datos del flujo

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user, id_proyecto,'ver_user_story'):
        proyecto = Proyecto.objects.get(id=id_proyecto)
        template_name = './UserStory/ver_user_story.html'
        us = User_Story.objects.get(id=id_user_story)
        listaComentario = Comentario.objects.filter(user_story = us).order_by("fecha")
        sizeEnDias= int(float(us.size)/6.0)
        return render(request, template_name, {'us': us,'id_user_story': id_user_story ,
                                           'id_proyecto': id_proyecto,'comentarios':listaComentario,
                                           'size':sizeEnDias,
                                           'proyecto':proyecto})
    else:
        template_name = '403_flujos_us.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def verHistorial(request, id_user_story, id_proyecto):
    """
    Recibe un request, el id del User Story que se desea conocer su historial y el id del proyecto por el cual se accedio,
    para luego mostrar el formulario de visualizacion del historial User Story.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_user_story : integer.
    :param id_user_story : Contiene el id del User Story a ser visualizado.

    :rtype: django.http.HttpResponse
    :return: ver_flujo.html, donde se muestran los datos del flujo

    :author: Eduardo Mendez
    """

    template_name = './UserStory/ver_historial_user_story.html'
    us = User_Story.objects.get(id=id_user_story)
    listaHistorial = Historial.objects.filter(user_story = us).order_by("id")

    return render(request, template_name, {'us': us,'id_user_story': id_user_story ,'id_proyecto': id_proyecto,'lista':listaHistorial})

@login_required(login_url='/login/')
def modificarUserStory(request,id_proyecto, id_user_story):
    """
    Vista para editar un User Story, se pueden editar los campos dependiendo del permiso que se posee, y de acuerdo a
    a la posicion del flujo, la actividad y el estado en el que se encuentran
    :return render_to_response:
    :return render:
    :param request:
    :param id_proyecto: id del proyecto que al que corresponde
    :param id_user_story: id del user story que se quiere editar

    """
    if puede_realizar_accion(request.user, id_proyecto, 'modificar_user_story'):
        today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        usuario_modificador = request.user.username
        proyecto = Proyecto.objects.get(id=id_proyecto)

        usModificado = User_Story.objects.get(id=id_user_story)
        fechaModificacion = today
        modificador = str(usuario_modificador)
        fecha = str(fechaModificacion)
        historico = 'El usuario '+modificador+ " en fecha "+fecha +" modifico: "
        cadenaMinima = len(fecha)+ len(modificador) + 32

        if request.method == 'POST':
            form = UserStoryModificadoForm(proyecto,request.POST)
            if form.is_valid():
                form.clean()
                nombre = form.cleaned_data['Nombre']
                if(usModificado.nombre != nombre): #cambia el nombre
                    historico = historico + "El nombre, antes "+ str(usModificado.nombre)+", ahora "+str(nombre)+ ", "
                    usModificado.nombre = nombre

                descripcion= form.cleaned_data['Descripcion']
                usModificado.descripcion = descripcion

                prioridad = form.cleaned_data['Prioridad']
                if(usModificado.prioridad != prioridad): #cambia la prioridad
                    historico = historico + "La prioridad, antes "+ str(usModificado.prioridad)+", ahora "+str(prioridad)+ ", "
                    usModificado.prioridad = prioridad

                valor_negocio = form.cleaned_data['Valor_De_Negocio']
                if(usModificado.valor_negocio != valor_negocio): #cambia el valor del negocio
                    historico = historico + "El valor de negocio, antes "+ str(usModificado.valor_negocio)+", ahora "+str(valor_negocio)+ ", "
                    usModificado.valor_negocio = valor_negocio


                valor_tecnico= form.cleaned_data['Valor_Tecnico']
                if(usModificado.valor_tecnico != valor_tecnico): #cambia el valor tecnico
                    historico = historico + "El valor tecnico, antes "+ str(usModificado.valor_tecnico)+", ahora "+str(valor_tecnico)+ ", "
                    usModificado.valor_tecnico = valor_tecnico

                size = form.cleaned_data['Size']
                sizeDias= int(float(usModificado.size)/6.0)

                if(sizeDias != size): #cambia el size
                    historico = historico + "El size, antes "+ str(sizeDias)+", ahora "+str(size) + ", "
                    size = size*6
                    usModificado.size = size


                usuario_asociado = form.cleaned_data['Usuario_Asociado']
                if(usModificado.usuario_asociado != usuario_asociado): #cambia el usuario del HU
                    historico = historico + "El usuario asociado, antes "+ str(usModificado.usuario_asociado)+", ahora "+str(usuario_asociado) + ", "
                    usModificado.usuario_asociado = usuario_asociado

                try:
                    usExistente = User_Story.objects.get(nombre=nombre, proyecto=proyecto,borrado_logico= False)
                except:
                    usExistente = None

                if usExistente:
                    template_name = './UserStory/modificar_user_story.html'
                    return render(request, template_name, {'form': form, 'mensaje' : 'User Story Duplicado dentro del Proyecto.', 'id_proyecto':id_proyecto})

                historialNuevo = Historial(descripcion=historico,user_story=usModificado)
                usModificado.save()

                if(len(historico)!= cadenaMinima):
                    historialNuevo.save()

                    send_mail("Asunto",
	                historico,
	                '"Administrador del Sistema"',
	                [proyecto.lider.email])

                template_name = './UserStory/user_story_modificado.html'
                return render(request, template_name, {'form': form, 'id_user_story': id_user_story, 'id_proyecto': id_proyecto})
        else:
            sizeEnDias= int(float(usModificado.size)/6.0)
            data = {'Nombre':usModificado.nombre ,'Descripcion':usModificado.descripcion,
	                'Prioridad':usModificado.prioridad,'Valor_De_Negocio':usModificado.valor_negocio,
	                'Valor_Tecnico':usModificado.valor_tecnico,'Size':sizeEnDias,
	                'Usuario_Asociado':usModificado.usuario_asociado.id}


            form = UserStoryModificadoForm(proyecto,data)
        template_name = './UserStory/modificar_user_story.html'
        return render(request, template_name, {'form': form, 'id_user_story': id_user_story, 'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_us.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def actualizarProgresoUserStory(request,id_proyecto, id_user_story):
    """
    Vista para actualizar el progreso de un User Story dentro de un proyecto, se pueden ir modificando los campos que envuelven al user Story
    en la logica del sistema
    :return render_to_response:
    :return render:
    :param request:
    :param id_user_story: id del HU que se quiere editar
    """
    if puede_realizar_accion(request.user,id_proyecto,'registrar_actividad'):
        usuario_modificador = request.user.username
        proyecto = Proyecto.objects.get(id=id_proyecto)
        today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")


        usModificado = User_Story.objects.get(id=id_user_story)
        fechaModificacion = today

        historico = 'El usuario '+str(usuario_modificador)+ " en fecha "+ str(fechaModificacion)+" modifico: "


        if request.method == 'POST':
            form = actualizarProgresoForm(usModificado.flujo,request.POST)
            if form.is_valid():
                form.clean()
                actividad = form.cleaned_data['Actividad']
                estado= form.cleaned_data['Estado']
                horas_dedicadas = form.cleaned_data['Horas_Dedicadas']
                comentario = form.cleaned_data['Comentario']

                if(usModificado.estado != estado):
                    if(usModificado.estado == 'TODO' and estado !='DOING'):
                        template_name = './UserStory/actualizar_progreso_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puede cambiar el estado a otro que no sea Doing',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    elif(usModificado.estado == 'DOING' and estado != 'DONE'):
                        template_name = './UserStory/modificar_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puedo cambiar el estado a otro que no sea Done',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    else:
                        ####################
                        historico = historico + "El estado, antes "+ str(usModificado.estado)+", ahora "+str(estado) + ", "

                        usModificado.estado = estado


                ultimaActividad = Actividad.objects.filter(flujo_id= usModificado.flujo.id).order_by('-orden')[0]
                order = usModificado.actividad.orden#orden de la actividad actual
                order = order+1
                if(usModificado.actividad != ultimaActividad):
                    actividadSgte = Actividad.objects.get(flujo = usModificado.flujo, orden= order)#traemos la actividad siguiente al orden actual

                if(usModificado.actividad!=actividad):#quiere cambiar de actividad
                    if(usModificado.actividad == ultimaActividad):
                        template_name = './UserStory/modificar_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puede cambiar la actividad porque es la ultima',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    elif(actividad !=actividadSgte):
                        template_name = './UserStory/modificar_user_story.html'
                        return render(request, template_name, {'form': form,
                                                           'mensaje': 'No puede cambiar a una actividad que no sea la siguiente',
                                                           'id_user_story': id_user_story,
                                                           'id_proyecto': id_proyecto})
                    else:
                        if(usModificado.estado != 'DONE'):
                            template_name = './UserStory/actualizar_progreso_user_story.html'
                            return render(request, template_name, {'form': form,
	                                                       'mensaje': 'No puede cambiar de actividad si el estado no es DONE',
	                                                       'id_user_story': id_user_story,
	                                                       'id_proyecto': id_proyecto})
                        else:
                            historico = historico + "La actividad, antes "+ str(usModificado.actividad)+", ahora "+str(actividad) + ", "
                            usModificado.actividad = actividad
                            historico = historico + "La estado, antes "+ str(usModificado.estado)+", ahora "+str(estado) + ", "
                            usModificado.estado = 'TODO'



                comentarioNuevo = Comentario(descripcion=comentario,user_story=usModificado,horas_dedicadas= horas_dedicadas,fecha=fechaModificacion)

                horas=usModificado.horas_dedicadas+horas_dedicadas
                historico = historico + "Las horas dedicadas, antes "+ str(usModificado.horas_dedicadas)+", ahora "+str(horas) + ", "
                usModificado.horas_dedicadas=horas
                usModificado.save()
                comentarioNuevo.save()
                historialNuevo = Historial(descripcion=historico,user_story=usModificado)
                historialNuevo.save()

                send_mail("Asunto",
	            historico,
	            '"Administrador del Sistema"',
	            [proyecto.lider.email])

                template_name = './UserStory/user_story_modificado.html'
                return render(request, template_name, {'form': form, 'id_user_story': id_user_story, 'id_proyecto': id_proyecto})
        else:
            data = {'Actividad':usModificado.actividad.id ,
                    'Estado':usModificado.estado}


            form = actualizarProgresoForm(usModificado.flujo,data)
        template_name = './UserStory/actualizar_progreso_user_story.html'
        return render(request, template_name, {'form': form, 'id_user_story': id_user_story, 'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_us.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def confEliminarUserStory(request,id_proyecto,id_user_story):
    """
    Recibe un request, la clave del proyecto y la clave del HU,
    para confirmar el borrado logico del HU en caso de ser posible.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_user_story : Contiene el id del HU que sera eliminado.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto al que corresponde.

    :rtype: django.http.HttpResponse
    :return: conf_eliminar_user_story.html, donde se dirige a la pantalla donde se confirma o no la operacion del borrado.

    :author: Eduardo Mendez
    """
    template_name = './UserStory/conf_eliminar_user_story.html'
    userStory = User_Story.objects.get(pk=id_user_story)

    return render(request, template_name, {'us': userStory, 'id_proyecto':id_proyecto})

@login_required(login_url='/login/')
def eliminarUserStory(request, id_proyecto,id_user_story):
    """
    Recibe un request, la clave del HU y la clave del proyecto por el cual se accedio,
    para realizar el borrado logico del HU en caso de ser posible.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_user_story : integer.
    :param id_user_story : Contiene el id del HU que sera eliminado.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto al que corresponde.

    :rtype: django.http.HttpResponse
    :return: user_story.html, donde se vuelve a la pantalla de administracion con el cambio hecho

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'eliminar_user_story'):
        eliminacionLogica = User_Story.objects.get(pk=id_user_story)
        eliminacionLogica.borrado_logico = True
        eliminacionLogica.save()
        template_name = './UserStory/eliminar_user_story.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_us.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})


def puede_realizar_accion(user,id_proyecto,permiso):
     #print(permiso)
    #print(id_proyecto)
    proyecto = Proyecto.objects.get(pk=id_proyecto)
    eq = Equipo.objects.filter(proyecto = proyecto , miembro = user)
    #print(len(eq))

    if user.is_superuser==True:
        return True

    if user == proyecto.lider:
        return True

    elif not eq==0:
        for miemb in eq:
            if miemb.rol.permissions.filter(Q(codename = permiso)):
                return True
            else:
                return False
