__author__ = 'eduardo'
from django.conf.urls import patterns, url
from .views import administrarUserStory,crearUserStory,modificarUserStory,asignarFlujo, verUserStory
from .views import eliminarUserStory, confEliminarUserStory,actualizarProgresoUserStory,verHistorial,finalizarUserStory
urlpatterns = patterns('',
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/$', administrarUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/nuevo/$', crearUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/modificar/(?P<id_user_story>\d+)/$', modificarUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/asignar_flujo/(?P<id_user_story>\d+)/$', asignarFlujo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/ver/(?P<id_user_story>\d+)/$', verUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/ver/(?P<id_user_story>\d+)/historial$', verHistorial),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/actualizar_trabajo/(?P<id_user_story>\d+)/$', actualizarProgresoUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/eliminar/(?P<id_user_story>\d+)/$', eliminarUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/confirmar_eliminar/(?P<id_user_story>\d+)/$', confEliminarUserStory),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_user_story/finalizar/(?P<id_user_story>\d+)/$', finalizarUserStory),

        )