__author__ = 'litatus'

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from apps.cliente.models import Cliente
from apps.cliente.views import administrarClientes, modificarCliente
from apps.cliente.views import cambiarEstado, clienteNuevo

class test_Autenticacion(TestCase):

    #Inicio las variables globables para los Tests.

    #Usuario administrador
    username = 'admin'
    password = 'admin'

    #Un usuario cualquiera
    usuario1 = 'litatus'
    contrasenha1 = '12345'

    #Un usuario inexistente
    usuarioX = 'lalala'
    contrasenhaX = 'lalala'

    #Cargo los usuarios de prueba para el Test
    fixtures = ['usuarios.json']

    def test_loguin_admin(self):
        """ Test para loguear al administrador. """

        resp = self.client.get('/login/')                                           #Solicitud de la pagina de autenticacion
        self.assertEqual(resp.status_code, 200)                                     #Pagina de login recibida con exito
        login = self.client.login(username=self.username, password=self.password)   #Proceso de autenticacion
        self.assertTrue(login)                                                      #Comprobamos si el usuario esta autenticado
        resp = self.client.get('/main/')                                            #Pasamos a la pagina de inicio
        self.assertEqual(resp.status_code, 200)                                     #Pagina de inicio recibida con exito
        resp = self.client.get('/adm_usuarios/')                                    #Pasamos a la pagina de administracion de usuarios
        self.assertEqual(resp.status_code, 200)                                     #Pagina de adm_usuarios recibida con exito
        self.assertTrue('lista_usuarios' in resp.context)                           #Comprobamos si recibimos la lista de usuarios
        logout= self.client.logout()                                                #Cerramos la sesion actual
        self.assertFalse(logout)                                                    #Probamos que efectivamente la sesion esta cerrada
        resp = self.client.get('/main/')                                    #Pasamos a la pagina de inicio
        self.assertNotEqual(resp.status_code, 200)                                  #Probamos que ya no podemos acceder al sistema si no estamos logueados
        print 'Test de login Usuario Administrador ejecutado exitosamente.'

    def test_loguin_user(self):
        """ Test para loguear a un usuario. """

        resp = self.client.get('/login/')                                           #Solicitud de la pagina de autenticacion
        self.assertEqual(resp.status_code, 200)                                     #Pagina de login recibida con exito
        login = self.client.login(username=self.usuario1, password=self.contrasenha1)               #Proceso de autenticacion
        self.assertTrue(login)                                                      #Comprobamos si el usuario esta autenticado
        resp = self.client.get('/main/')                                            #Pasamos a la pagina de inicio
        self.assertEqual(resp.status_code, 200)                                     #Pagina de inicio recibida con exito
        resp = self.client.get('/adm_usuarios/')                                    #Pasamos a la pagina de administracion de usuarios
        self.assertEqual(resp.status_code, 200)                                     #Pagina de adm_usuarios recibida con exito
        self.assertTrue('lista_usuarios' in resp.context)                           #Comprobamos si recibimos la lista de usuarios
        logout= self.client.logout()                                                #Cerramos la sesion actual
        self.assertFalse(logout)                                                    #Probamos que efectivamente la sesion esta cerrada
        resp = self.client.get('/adm_proyectos/')                                   #Pasamos a la pagina de inicio
        self.assertNotEqual(resp.status_code, 200)                                  #Probamos que ya no podemos acceder al sistema si no estamos logueados
        print 'Test de login usuario normal ejecutado exitosamente.'

    def test_loguin_userAnonimo(self):
        """ Test para loguear a un usuario no registardo. """


        resp = self.client.get('/login/')                                           #Solicitud de la pagina de autenticacion
        self.assertEqual(resp.status_code, 200)                                     #Pagina de login recibida con exito
        login = self.client.login(username=self.usuarioX, password=self.contrasenhaX)              #Proceso de autenticacion
        self.assertFalse(login)                                                     #Comprobamos si el usuario esta autenticado
        resp = self.client.get('/main/')                                                 #Pasamos a la pagina de inicio
        self.assertEqual(resp.status_code, 302)                                     #Pagina de inicio recibida con exito
        resp = self.client.get('/main/')                                    #Pasamos a la pagina de administracion de usuarios
        self.assertEqual(resp.status_code, 302)                                     #Pagina de adm_usuarios recibida con exito
        print 'Test de intento de login usuario no registrado en Base de Datos, ejecutado exitosamente.'

    def test_ingresar_sin_registrarse(self):
        """ Error 302 "movido Temporalmente",
            al ingresar intentar ingresar a la pagina
            de inicio sin antes habernos logueado
            el error 302 nos imposibilita el acceso """


        resp = self.client.get('/')                                                 #Pasamos a la pagina de inicio
        self.assertEqual(resp.status_code, 302)                                     #Ocurre el erro 302
        resp = self.client.get('/main/')                                    #Pasamos a la pagina de administracion de usuarios
        self.assertEqual(resp.status_code, 302)                                     #Ocurre el error 302
        print 'Test de intento de acceso de usuario no Loggeado, ejecutado exitosamente.'

    if __name__ == '__main__':
        unittest.main()
