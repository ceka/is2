from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.views.generic import TemplateView
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
import logging
from apps.autenticacion.forms import LoginForms
from skp.settings import DEFAULT_FROM_EMAIL

logger = logging.getLogger(__name__)

class Autenticacion(TemplateView):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect('login')
        else:
            return HttpResponseRedirect('/main')


def login_vista(request):
    mensaje=''
    if request.method == "POST":
        form = LoginForms(request.POST)
        if form.is_valid():
            username = form.cleaned_data['Usuario']
            password = form.cleaned_data['Clave']
            usuario = authenticate(username=username, password=password)
            if usuario is not None and usuario.is_active:
                login(request, usuario)
                logger.info('Login de Usuario %s' % request.user.username)
                return HttpResponseRedirect('/')
            else:
                mensaje = 'Disculpa, el Nombre de Usuario o la Clave no coinciden.'
    formulario = LoginForms()
    ctx = {'formulario':formulario, 'mensaje':mensaje}
    return render_to_response ('autenticacion/ingresar.html', ctx, context_instance=RequestContext(request))


def logout_vista (request):
    logout(request)
    logger.info('Logout de Usuario %s' % request.user.username)
    return HttpResponseRedirect('/')

def UserResetPassword(request):
    if request.method == 'POST':
        return password_reset(request, from_email=DEFAULT_FROM_EMAIL)
    else:
        return render(request, 'autenticacion/forgot_password.html')


@login_required(login_url='/login/')
def admin_proy(request):
    return render_to_response ('pagina_principal/main.html', '', context_instance=RequestContext(request))