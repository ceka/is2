from apps.autenticacion.views import login_vista, Autenticacion, logout_vista, admin_proy


__author__ = 'hdmedina'

from django.conf.urls import patterns, url
from django.contrib.auth import views as auth_views


urlpatterns = patterns('',

    url(r'^$', Autenticacion.as_view(), name='inicio'),
    url(r'^login/$', login_vista, name='login'),
    url(r'^cerrar/$', logout_vista, name='logout'),
    url(r'^main/$', admin_proy),
    url(r'^forgot_password/$', 'django.contrib.auth.views.password_reset', {'template_name':'autenticacion/forgot_password.html',\
                               'post_reset_redirect' : 'registration/password_reset_done'}, name="reset_password"),
    url(r'^forgot_password/registration/password_reset_done/$', 'django.contrib.auth.views.password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'django.contrib.auth.views.password_reset_confirm',
    name='password_reset_confirm'),
    url(r'^user/password/reset/confirm/$',
             'django.contrib.auth.views.password_reset_confirm'),
    url(r'^password/reset/complete/$',
    'django.contrib.auth.views.password_reset_confirm', name='password_reset_complete'),



)