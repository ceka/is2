__author__ = 'litatus'
from django import forms
from django.core.exceptions import ValidationError
from apps.cliente.models import Cliente
from django.contrib.auth.models import User

def validate_username_unique(value):
    if Cliente.objects.filter(empresa=value).exists():
        raise ValidationError(u'El Cliente ya existe en la Base de Datos')

class ClienteNuevoForm (forms.Form):
    """
    Estos son los campos que se cargan durante el registro de un Cliente Nuevo, que luego pasa al template html
    que recibe los datos para luego persistir.
    Adicionalmente Controla los datos ingresados por el usuario.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Miguel Pereira
    """

    Empresa = forms.CharField(widget=forms.TextInput(), validators=[validate_username_unique], max_length=30, required=True, error_messages={'required': 'Ingrese Empresa', })
    RUC = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese RUC', })
    Telefono = forms.IntegerField()
    Direccion = forms.CharField(widget=forms.TextInput())

class ClienteModificadoForm (forms.Form):
    """
    Estos son los campos que se cargan durante la modificacion de un Cliente Existente, que luego pasa al template html
    que recibe los datos para luego persistir.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Miguel Pereira
    """

    Empresa = forms.CharField(widget=forms.TextInput(), max_length=30, required=True, error_messages={'required': 'Ingrese un nombre de Empresa', 'max_length': 'Longitud maxima: 30', 'min_length': 'Longitud minima: 5 caracteres'})
    RUC = forms.CharField(widget=forms.TextInput(), max_length=10, min_length=5, required=True, error_messages={'required': 'Ingrese el RUC', 'max_length': 'Longitud maxima: 10', 'min_length': 'Longitu minima: 5 caracteres',})
    Telefono = forms.IntegerField(widget = forms.TextInput(),error_messages = {'required':'Ingrese el telefono de la empresa'})
    Direccion = forms.CharField(widget=forms.TextInput())

    def clean(self):
        super(forms.Form, self).clean()
        return self.cleaned_data





