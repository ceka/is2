__author__ = 'litatus'

from django.db import models
from django.contrib.auth.models import User

class Cliente(models.Model):
    """
    Contiene los campos de la tabla Cliente en la Base de Datos

    :variables:
    -   Empresa: Nombre legal de la empresa
    -   RUC: Registro Unico del contribuyente
    -   Usuario Asignado: Usuario que le corresponde para ingresar al sistema
    -   is_active: booleano para realizar el borrado logico

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Miguel Pereira
    """

    empresa = models.CharField(max_length=30, null=True)
    ruc = models.CharField(max_length=10, null=True)
    telefono = models.IntegerField( )
    direccion = models.CharField(max_length=100, null=True)
    borrado_logico = models.BooleanField(default=False)


    def __unicode__(self):
        return unicode(self.empresa)

    class Meta:
        verbose_name = 'cliente'
        verbose_name_plural = 'clientes'
        permissions = (
            ('listar_cliente', 'Puede listar cliente'),
            ('crear_cliente', 'Puede crear cliente'),
            ('cambiar_estado_cliente', 'Puede cambiar estado del cliente'),
            ('modificar_cliente', 'Puede modificar cliente'),
        )