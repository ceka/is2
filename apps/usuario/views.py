__author__ = 'eduardo'
from django.views.generic import TemplateView
from django.contrib.auth.models import User, Permission
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template.context import RequestContext
from django.contrib.auth.hashers import check_password, make_password
from forms import UsuarioNuevoForm, UsuarioModificadoForm
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from collections import OrderedDict


@login_required(login_url='/login/')
def administrarUsuarios(request):
    usuario_actor = request.user
    lista_usuarios = User.objects.all().order_by('id')
    ctx = {'usuario_actor':usuario_actor,'lista_usuarios':lista_usuarios}
    return render_to_response('Usuario/usuarios.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def usuarioNuevo(request):
    if request.method == 'POST':
        form = UsuarioNuevoForm(request.POST)
        if form.is_valid():
            form.clean()
            username = form.cleaned_data['Usuario']
            password = form.cleaned_data['Contrasenha']
            password2 = form.cleaned_data['Reingresar_Contrasenha']
            email = form.cleaned_data['Email']
            first_name = form.cleaned_data['Nombre']
            last_name = form.cleaned_data['Apellido']
            superUser = form.cleaned_data['Super_Usuario']

            if (password != password2):
                template_name = './Usuario/usuarionuevo.html'
                return render(request, template_name, {'form': form, 'mensaje': 'el password no coincide'})


            userNuevo = User.objects.create_user(username, email, password)
            userNuevo.first_name = first_name
            userNuevo.last_name = last_name
            userNuevo.is_superuser = superUser
            userNuevo.save()

            template_name = './Usuario/usuariocreado.html'
            return render(request, template_name)


    else:
        form = UsuarioNuevoForm()

    template_name = './Usuario/usuarionuevo.html'
    return render(request, template_name, {'form': form})

@login_required(login_url='/login/')
def modificarUsuario(request, id_usuario):


    modificador = User.objects.get(username=request.user.username)

    usuarioModificado = User.objects.get(id=id_usuario)
    if request.method == 'POST':
        form = UsuarioModificadoForm(request.POST)
        if form.is_valid():
            form.clean()
            username = form.cleaned_data['Usuario']
            password = form.cleaned_data['Contrasenha']
            nuevoPassword = form.cleaned_data['Reingresar_Contrasenha']
            email = form.cleaned_data['Email']
            first_name = form.cleaned_data['Nombre']
            last_name = form.cleaned_data['Apellido']

            if password:
                if check_password(password, usuarioModificado.password):
                    password = make_password(nuevoPassword)
                else:
                    template_name = './Usuario/usuarioalerta.html'
                    return render(request, template_name, {'form': form,'mensaje': 'las contrasenhas no coinciden'})
            else:
                password = usuarioModificado.password

            usuarioModificado.username = username
            usuarioModificado.password = password
            usuarioModificado.email = email
            usuarioModificado.first_name = first_name
            usuarioModificado.last_name = last_name
            usuarioModificado.save()


            template_name = './Usuario/usuario_modificado.html'
            return render(request, template_name)
    else:
        data = {'Usuario': usuarioModificado.username, 'Contrasenha': '', 'Reingresar_Contrasenha': '',
                'Email': usuarioModificado.email, 'Nombre': usuarioModificado.first_name, 'Apellido': usuarioModificado.last_name}
        form = UsuarioModificadoForm(data)
    template_name = './Usuario/modificar_usuario.html'
    return render(request, template_name, {'form': form, 'id_usuario': id_usuario})

    #permisonDenied?


@login_required(login_url='/login/')
def verUsuario(request, id_usuario):
    template_name = './Usuario/consultar_usuario.html'
    usuario = User.objects.get(pk=id_usuario)

    return render(request, template_name, {'usuario': usuario, 'id_usuario': id_usuario})


@login_required(login_url='/login/')
#@permission_required('usuarios.eliminar_usuarios', raise_exception=True)
def cambiarEstado(request, id_usuario):
    eliminacionLogica = User.objects.get(pk=id_usuario)
    if eliminacionLogica.is_active:
        #FALTA LO DE PROYECTO AGREGAR
        eliminacionLogica.is_active = False
        eliminacionLogica.save()
        return HttpResponseRedirect('/adm_usuarios/')

    else:
        eliminacionLogica.is_active = True
        eliminacionLogica.save()
        return HttpResponseRedirect('/adm_usuarios/')







