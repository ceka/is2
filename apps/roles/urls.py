from apps.roles.views import crear_rol, listar_roles, eliminar_rol, RegisterSuccessView, editar_rol, detalle_rol, \
    buscarRol

__author__ = 'hdmedina'
from django.conf.urls import patterns, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^crear/$', crear_rol),
    url(r'^$', listar_roles),
    url(r'^eliminar/(?P<id_rol>\d+)$', eliminar_rol),
    url(r'^register/success/$', RegisterSuccessView.as_view()),
    url(r'^modificar/(?P<id_rol>\d+)$', editar_rol),
    url(r'^ver_detalle/(?P<id_rol>\d+)$', detalle_rol),
    url(r'^search/$',buscarRol, name='buscar_roles'),
    )
