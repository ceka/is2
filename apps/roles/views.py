from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group, Permission
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.views.generic import TemplateView
from apps.roles.forms import GroupForm
from skp import settings


__author__ = 'hdmedina'


@login_required
  
def crear_rol(request):
    '''
    vista para crear un roles, que consta de un nombre y una lista de permisos
    '''
    if request.method == 'POST':
        # formulario enviado
        group_form = GroupForm(request.POST)

        if group_form.is_valid():
            # formulario validado correctamente
            group_form.save()
            return HttpResponseRedirect('/roles/register/success/')

    else:
        # formulario inicial
        group_form = GroupForm()
    return render_to_response('roles/crear_rol.html', { 'group_form': group_form}, context_instance=RequestContext(request))

@login_required
def listar_roles(request):
    '''
    vista para listar los roles exitentes en el sistema
    '''

    grupos = Group.objects.all()
    return render_to_response('roles/listar_roles.html', {'datos': grupos}, context_instance=RequestContext(request))

@login_required
def buscarRol(request):
    '''
    vista para buscar un rol entre todos los registrados en el sistema
    '''
    query = request.GET.get('q', '')
    if query:
        qset = (
            Q(name=query)
        )
        results = Group.objects.filter(qset).distinct()
    else:
        results = []
    return render_to_response('roles/listar_roles.html', {'datos': results}, context_instance=RequestContext(request))

@login_required
def detalle_rol(request, id_rol):

    '''
    vista para ver los detalles {% comment %}del rol <id_rol> del sistema
    '''

    dato = get_object_or_404(Group, pk=id_rol)
    permisos = Permission.objects.filter(group=id_rol)
    return render_to_response('roles/detalle_rol.html', {'rol': dato, 'permisos': permisos}, context_instance=RequestContext(request))

@login_required
def eliminar_rol(request, id_rol):

    '''
    vista para eliminar el rol <id_rol>. Se comprueba que dicho rol no tenga fases asociadas.
    :param request:
    :param id_rol:
    '''

    dato = get_object_or_404(Group, pk=id_rol)
    if dato.name=='Lider':
        messages.add_message(request, settings.DELETE_MESSAGE, "El rol lider no se puede eliminar")
    else:
        dato.delete()
        messages.add_message(request, settings.DELETE_MESSAGE, "Rol eliminado")
    grupos = Group.objects.all()
    return render_to_response('roles/listar_roles.html', {'datos': grupos}, context_instance=RequestContext(request))

class RegisterSuccessView(TemplateView):
    template_name = 'roles/creacion_correcta.html'

@login_required
def editar_rol(request,id_rol):
    '''
    vista para cambiar el nombre del rol o su lista de permisos.
    '''
    rol= Group.objects.get(id=id_rol)
    if request.method == 'POST':
        # formulario enviado
        rol_form = GroupForm(request.POST, instance=rol)

        if rol_form.is_valid():
            # formulario validado correctamente
            rol_form.save()
            return HttpResponseRedirect('/roles/register/success/')

    else:
        # formulario inicial
        rol_form = GroupForm(instance=rol)
    return render_to_response('roles/editar_rol.html', { 'rol': rol_form, 'dato':rol}, context_instance=RequestContext(request))
