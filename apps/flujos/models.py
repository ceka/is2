__author__ = 'eduardo'
from django.db import models
from apps.proyecto.models import Proyecto

class Flujo(models.Model):
    """
    Contiene los campos de la tabla Actividad en la Base de Datos.
    El nombre del flujo.
    El estado del flujo para su eliminacion logica.


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    nombre = models.CharField(max_length=50)
    borrado_logico = models.BooleanField(default=False)
    proyecto = models.ForeignKey(Proyecto)


    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'flujo'
        verbose_name_plural = 'flujos'
        permissions = (
            ('listar_flujo', 'Puede listar flujo'),
            ('crear_flujo', 'Puede crear flujo'),
            ('modificar_flujo','Puede modificar flujo'),
            ('eliminar_flujo','Puede eliminar flujo'),
            ('administrar_flujos','Puede administrar flujos'),
            ('ver_flujo','Puede ver flujos'),
        )

class Actividad(models.Model):
    """
    Contiene los campos de la tabla Actividad en la Base de Datos.
    El orden de una actividad dentro del flujo.
    El nombre de la actividad dentro del flujo.
    El Flujo al que corresponde la actividad creada.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    orden = models.IntegerField()
    nombre = models.CharField(max_length=50)
    flujo = models.ForeignKey(Flujo)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'actividad'
        verbose_name_plural = 'actividades'
        permissions = (
            ('listar_actividad', 'Puede listar actividad'),
            ('crear_actividad','Puede crear actividad'),
            ('ver_actividad','Puede ver actividad'),
            ('eliminar_actividad','Puede eliminar actividad'),
            ('modificar_actividad','Puede modificar actividad')
        )