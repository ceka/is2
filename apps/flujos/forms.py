__author__ = 'eduardo'
from django.forms import ModelForm
from django import forms
from apps.flujos.models import Flujo, Actividad
from django.core.exceptions import ValidationError



class FlujoNuevoForm(forms.Form):
    """
    Este es el unico campo que se carga durante el registro de un Flujo Nuevo, que luego pasa al template html
    que recibe los datos para luego persistir.


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """
    Nombre_Flujo = forms.CharField(widget=forms.TextInput(),  max_length=50, required=True, error_messages={'required': 'Ingrese un nombre del Flujo', 'max_length': 'Longitud maxima: 50 caracteres'})

class FlujoModificadoForm(forms.Form):
    """
    Este es el campo que se carga durante la modificacion de un Flujo Nuevo, que luego pasa al template html
    que recibe los datos para luego persistir.


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    Nombre_Flujo = forms.CharField(widget=forms.TextInput(), max_length=50, error_messages={'required': 'Ingrese un nombre del Flujo', 'max_length': 'Longitud maxima: 50 caracteres'})


class ActividadNuevaForm(forms.Form):
    """
    Estos son los campos que se cargan durante la creacion de una Actividad Nueva asociada a un Flujo,
    que luego pasa al template html que recibe los datos para luego persistir.


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    Nombre_Actividad = forms.CharField(widget=forms.TextInput(), label="Nombre", max_length=50, required=True, error_messages={'required': 'Ingrese el nombre de la actividad'})

class ActividadModificadaForm(forms.Form):
    """
    Estos son los campos que se cargan durante la modifacion de una Actividad Nueva asociada a un Flujo,
    que luego pasa al template html que recibe los datos para luego persistir.


    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    Orden_Actividad = forms.IntegerField(required=True, error_messages={'required': 'Ingrese un nombre del Flujo'})
    Nombre_Actividad = forms.CharField(widget=forms.TextInput(), label="Nombre", max_length=50, required=True, error_messages={'required': 'Ingrese el nombre de la actividad'})
