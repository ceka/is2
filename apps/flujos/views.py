from django.db.models import Q

__author__ = 'eduardo'

from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from apps.flujos.models import Flujo, Actividad
from apps.proyecto.models import Proyecto, Equipo
from apps.flujos.forms import FlujoNuevoForm, FlujoModificadoForm, ActividadNuevaForm, ActividadModificadaForm

@login_required(login_url='/login/')
def administrarFlujos(request,id_proyecto):
    """
    Se recibe un request y el id del proyecto por el cual se accedio a la vista, y se listan los flujos creados
    que corresponden a ese proyecto, para luego pasar al HTML que muestra la vista.
    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: flujos.html, donde se listan los flujos, ademas de las funcionalidades para los flujos

    :author: Eduardo Mendez
    """
    usuario_actor = request.user
    lista_flujos = Flujo.objects.filter(proyecto= id_proyecto, borrado_logico= False).order_by('id') #trae todos, hasta los inactivos
   #lista_flujos = Flujo.objects.filter(estado= True) #trae solo los de estado true

    ctx = {'usuario_actor':usuario_actor,'lista_flujos':lista_flujos, 'id_proyecto':id_proyecto}
    return render_to_response('Flujo/flujos.html', ctx, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def crearFlujo(request,id_proyecto):
    """
    Recibe un request y el id del proyecto por el cual se accedio, para luego mostrar el formulario de creacion
    de un flujo nuevo, verifica la integridad de los datos cargados, crea y guarda el flujo nuevo.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: flujo_creado.html, donde se muestra un mensaje de creacion exitosa

    :author: Eduardo Mendez
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)

    if request.method == 'POST':
        form = FlujoNuevoForm(request.POST)
        if form.is_valid():
            form.clean()
            nombreFlujo = form.cleaned_data['Nombre_Flujo']

            try:
                flujoExistente = Flujo.objects.get(nombre=nombreFlujo,proyecto = proyecto,borrado_logico=False)
            except:
                flujoExistente = None

            if flujoExistente:
                template_name = './Flujo/crear_flujo.html'
                return render(request, template_name, {'form': form,'mensaje':'Flujo Duplicado dentro del Proyecto'})

            flujoNuevo = Flujo(nombre=nombreFlujo, proyecto=proyecto)
            flujoNuevo.save()


            template_name = './Flujo/flujo_creado.html'
            return render(request, template_name, {'id_proyecto': id_proyecto})


    else:
        form = FlujoNuevoForm()

    template_name = './Flujo/crear_flujo.html'
    return render(request, template_name, {'form': form})

@login_required(login_url='/login/')
def modificarFlujo(request, id_flujo,id_proyecto):
    """
    Recibe un request, el id del flujo que se modifica y el id del proyecto por el cual se accedio,
    para luego mostrar el formulario de modificacion de un flujo nuevo, verifica la integridad de
    los datos cargados, se guardan los cambios en el flujo.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo que sera modificado.

    :rtype: django.http.HttpResponse
    :return: flujo_modificado.html, donde se muestra un mensaje de modificacion exitosa

    :author: Eduardo Mendez
    """

    proyecto = Proyecto.objects.get(id=id_proyecto)
   # modificador = User.objects.get(username=request.user.username)
    if puede_realizar_accion(request.user,id_proyecto,'modificar_flujo'):
        flujoModificado = Flujo.objects.get(id=id_flujo)
        if request.method == 'POST':
            form = FlujoModificadoForm(request.POST)
            if form.is_valid():
                form.clean()
                nombreFlujo = form.cleaned_data['Nombre_Flujo']

                try:
                    flujoExistente = Flujo.objects.get(nombre=nombreFlujo,proyecto = proyecto,borrado_logico=False)
                except:
                    flujoExistente = None

                if flujoExistente:
                    template_name = './Flujo/modificar_flujo.html'
                    return render(request, template_name, {'form': form,'mensaje':'Flujo Duplicado dentro del Proyecto'})



                flujoModificado.nombre = nombreFlujo
                flujoModificado.save()

                template_name = './Flujo/flujo_modificado.html'
                return render(request, template_name, {'form': form, 'id_flujo': id_flujo, 'id_proyecto': id_proyecto})
        else:
            data = {'Nombre_Flujo': flujoModificado.nombre}
            form = FlujoModificadoForm(data)
        template_name = './Flujo/modificar_flujo.html'
        return render(request, template_name, {'form': form, 'id_flujo': id_flujo, 'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def verFlujo(request, id_flujo,id_proyecto):
    """
    Recibe un request, el id del flujo que se desea visualizar y el id del proyecto por el cual se accedio,
    para luego mostrar el formulario de visualizacion de un flujo.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo a ser visualizado.

    :rtype: django.http.HttpResponse
    :return: ver_flujo.html, donde se muestran los datos del flujo

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'ver_flujo'):
        template_name = './Flujo/ver_flujo.html'
        flujo = Flujo.objects.get(pk=id_flujo)

        return render(request, template_name, {'flujo': flujo,'id_flujo': id_flujo ,'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def confEliminarFlujo(request, id_flujo,id_proyecto):
    """
    Recibe un request, la clave del flujo y la clave del proyecto por el cual se accedio,
    para confirmar el borrado logico del flujo en caso de ser posible.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo que sera eliminado.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto al que corresponde.

    :rtype: django.http.HttpResponse
    :return: conf_eliminar_flujo.html, donde se dirige a la pantalla donde se confirma o no la operacion del borrado.

    :author: Eduardo Mendez
    """
    template_name = './Flujo/conf_eliminar_flujo.html'
    flujo = Flujo.objects.get(pk=id_flujo)

    return render(request, template_name, {'flujo': flujo, 'id_proyecto':id_proyecto})

@login_required(login_url='/login/')
def eliminarFlujo(request, id_flujo, id_proyecto):
    """
    Recibe un request, la clave del flujo y la clave del proyecto por el cual se accedio,
    para realizar el borrado logico del flujo en caso de ser posible.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo que sera eliminado.

    :type id_proyecto : integer.
    :param id_proyecto : Contiene el id del proyecto al que corresponde.

    :rtype: django.http.HttpResponse
    :return: adm_flujo.html, donde se vuelve a la pantalla de administracion con el cambio hecho

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'eliminar_flujo'):
        eliminacionLogica = Flujo.objects.get(pk=id_flujo)
        eliminacionLogica.borrado_logico = True
        eliminacionLogica.save()
        template_name = './Flujo/eliminar_flujo.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def administrarActividades(request, id_flujo, id_proyecto):
    """
    Se recibe un request y el id del flujo por el cual se accedio a la vista, y se listan las actividades creadas
    que corresponden a ese flujo, para luego pasar al HTML que muestra la vista.

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo al que corresponde.

    :rtype: django.http.HttpResponse
    :return: actividades.html, donde se listan las actividades, ademas de las funcionalidades para las actividades

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'listar_actividad'):
        usuario_actor = request.user
        lista_actividades = Actividad.objects.filter(flujo=id_flujo) #trae solo los de ese flujo

        ctx = {'usuario_actor':usuario_actor,'lista_actividades':lista_actividades, 'id_proyecto':id_proyecto }
        return render_to_response('Flujo/Actividad/actividades.html', ctx, context_instance=RequestContext(request))
    else:
        template_name = '403_flujos.html'
        return render(request, template_name, {'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def crearActividad(request, id_flujo,id_proyecto):
    """
    Recibe un request y el id del flujo por el cual se accedio, para luego mostrar el formulario de creacion
    de una actividad nueva, verifica la integridad de los datos cargados, crea y guarda la actividad nueva.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo al que corresponde.

    :rtype: django.http.HttpResponse
    :return: actividad_creada.html, donde se muestra un mensaje de creacion exitosa


    :author: Eduardo Mendez
    """

    flujo = Flujo.objects.get(pk=id_flujo)
    if request.method == 'POST':
        form = ActividadNuevaForm(request.POST)
        if form.is_valid():
            form.clean()
            nombreActividad = form.cleaned_data['Nombre_Actividad']

            try:
                ultimaActividad = Actividad.objects.filter(flujo_id= id_flujo).order_by('-orden')[0] #tengo que saber si del flujo en el que esta actualmente,se llego a la ultima actividad
                ultimoOrden = ultimaActividad.orden
            except :
                ultimoOrden = 0
            ultimoOrden = ultimoOrden + 1

            try:
                actividadExistente = Actividad.objects.get(nombre=nombreActividad,flujo = flujo)
            except:
                actividadExistente = None

            if actividadExistente:
                template_name = './Flujo/Actividad/crear_actividad.html'
                return render(request, template_name, {'form': form,'mensaje':'Actividad Duplicado dentro del Flujo' ,'id_flujo': id_flujo,'id_proyecto': id_proyecto})


            actividadNueva = Actividad(orden=ultimoOrden, nombre=nombreActividad, flujo=flujo)
            actividadNueva.save()

            template_name = './Flujo/Actividad/actividad_creada.html'
            return render(request, template_name, {'id_flujo': id_flujo,'id_proyecto': id_proyecto})


    else:
        form = ActividadNuevaForm()

    template_name = './Flujo/Actividad/crear_actividad.html'
    return render(request, template_name, {'form': form, 'id_flujo': id_flujo,'id_proyecto': id_proyecto})

@login_required(login_url='/login/')
def modificarActividad(request, id_actividad, id_flujo,id_proyecto):
    """
    Recibe un request y el id del flujo por el cual se accedio, para luego mostrar el formulario de modificacion
    de una actividad, verifica la integridad de los datos cargados, modifica y guarda la actividad seleccionada.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo al que corresponde.

    :type id_actividad : integer.
    :param id_actividad : Contiene el id de la actividad a ser modificada.

    :rtype: django.http.HttpResponse
    :return: actividad_modificada.html, donde se muestra un mensaje de modificacion exitosa

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'modificar_actividad'):
        flujo = Flujo.objects.get(pk=id_flujo)

       # modificador = User.objects.get(username=request.user.username)

        actividadModificada = Actividad.objects.get(id=id_actividad)
        if request.method == 'POST':
            form = ActividadModificadaForm(request.POST)
            if form.is_valid():
                form.clean()
                ordenActividad = form.cleaned_data['Orden_Actividad']
                nombreActividad = form.cleaned_data['Nombre_Actividad']


                try:
                    actividadExistente = Actividad.objects.get(nombre=nombreActividad,flujo = flujo)
                except:
                    actividadExistente = None

                if actividadExistente:
                    template_name = './Flujo/Actividad/modificar_actividad.html'
                    return render(request, template_name, {'form': form,'mensaje':'Actividad Duplicado dentro del Flujo' ,'id_flujo': id_flujo,'id_proyecto': id_proyecto})



                actividadModificada.pk = id_actividad
                actividadModificada.orden = ordenActividad
                actividadModificada.nombre = nombreActividad
                actividadModificada.flujo = flujo
                actividadModificada.save()

                template_name = './Flujo/Actividad/actividad_modificada.html'
                return render(request, template_name, {'id_flujo': id_flujo,'id_proyecto': id_proyecto} )
        else:
            data = {'Orden_Actividad': actividadModificada.orden,
                    'Nombre_Actividad': actividadModificada.nombre,
                    'Flujo_Correspondiente': actividadModificada.flujo}
            form = ActividadModificadaForm(data)
        template_name = './Flujo/Actividad/modificar_actividad.html'
        return render(request, template_name, {'form': form,
                                               'id_actividad': id_actividad,
                                               'id_flujo': id_flujo,
                                               'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_actividades.html'
        return render(request, template_name, {'id_proyecto': id_proyecto, 'id_flujo':id_flujo})

@login_required(login_url='/login/')
def verActividad(request, id_actividad,id_flujo,id_proyecto):
    """
    Recibe un request y el id del flujo por el cual se accedio, para luego mostrar el formulario de visualizacion
    de la actividad seleccionada.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo al que corresponde.

    :type id_actividad : integer.
    :param id_actividad : Contiene el id de la actividad a ser visualizada.

    :rtype: django.http.HttpResponse
    :return: ver_actividad.html, donde se muestran los datos a ser visualizados

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'ver_actividad'):
        template_name = './Flujo/Actividad/ver_actividad.html'
        actividad = Actividad.objects.get(pk=id_actividad)

        return render(request, template_name, {'actividad': actividad,
                                           'id_actividad': id_actividad,
                                           'id_flujo': id_flujo,
                                           'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_actividades.html'
        return render(request, template_name, {'id_proyecto': id_proyecto, 'id_flujo':id_flujo})

@login_required(login_url='/login/')
def eliminarActividad(request, id_actividad, id_flujo,id_proyecto):
    """
    Recibe un request, el id del flujo por el cual se accedio y el id de la actividad que sera eliminada,
    para luego mostrar el formulario de eliminacion.

    :type request: django.http.HttpRequest
    :param request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :type id_flujo : integer.
    :param id_flujo : Contiene el id del flujo al que corresponde.

    :type id_actividad : integer.
    :param id_actividad : Contiene el id de la actividad a ser eliminada.

    :rtype: django.http.HttpResponse
    :return: ver_actividad.html, donde se muestran los datos a ser visualizados

    :author: Eduardo Mendez
    """
    if puede_realizar_accion(request.user,id_proyecto,'eliminar_actividad'):
        actividad = Actividad.objects.get(pk=id_actividad)
        actividad.delete()

        template_name = './Flujo/Actividad/eliminar_actividad.html'
        return render(request, template_name, {'id_flujo': id_flujo,
                                           'id_proyecto': id_proyecto})
    else:
        template_name = '403_flujos_actividades.html'
        return render(request, template_name, {'id_proyecto': id_proyecto, 'id_flujo':id_flujo})

def puede_realizar_accion(user,id_proyecto,permiso):
    #print(permiso)
    #print(id_proyecto)
    proyecto = Proyecto.objects.get(pk=id_proyecto)
    eq = Equipo.objects.filter(proyecto = proyecto , miembro = user)
    #print(len(eq))

    if user.is_superuser==True:
        return True

    if user == proyecto.lider:
        return True

    elif not eq==0:
        for miemb in eq:
            if miemb.rol.permissions.filter(Q(codename = permiso)):
                return True
            else:
                return False
