__author__ = 'litatus'


from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from apps.flujos.views import administrarFlujos, crearFlujo, modificarFlujo, verFlujo
from apps.flujos.views import eliminarFlujo, administrarActividades, crearActividad
from apps.proyecto.models import Proyecto
from apps.flujos.models import Flujo, Actividad

class test_flujo(TestCase):
    """    Cargamos los datos de prueba en la base de datos temporal para el test   """
    fixtures = ['db_dump.json']

    def setUp(self):
        """ Inicializamos la variable factory que posteriormente nos permitira cargar
            un request para utilizarlo en las vista.
        """
        self.factory = RequestFactory()


    def test_administrarFlujos(self):

        self.user = User.objects.get(pk=1)              #Elijo el usuario Administrador

        #Solicito en el request ir a los Flujos de un Proyecto
        request = self.factory.get('/adm_proyectos/1/adm_flujos/')
        request.user = self.user
        proyecto =  Proyecto.objects.get(id=1)          #elijo el proyecto con id=1
        self.assertTrue(proyecto)                       #verifico que exista el proyecto

        #Pasamos a la pagina de administracion de Flujos del Proyecto
        response = administrarFlujos(request, proyecto.id)
        self.assertEqual(response.status_code, 200)
        print '\nTest de Administracion de Flujos ejecutado exitosamente.\n'

    def test_crearFlujo(self):

        #imprimo la lista completa de Flujos antes de agregar el nuevo
        print Flujo.objects.all()

        # Crea una instancia de una solicitud GET
        self.user = User.objects.get(pk=1)

        #completamos en el request el formulario para un nuevo Flujo en el Proyecto con id=1
        request = self.factory.post('/adm_proyectos/1/adm_flujos/nuevo/', {'Nombre_Flujo': 'FlujoTEST'})
        request.user = self.user
        response = crearFlujo(request, 1)                   #mando a la vista de creacion de nuevo Flujo
        self.assertEqual(response.status_code, 200)         #verifico que se haya recibido la pagina
        busco = Flujo.objects.get(nombre='FlujoTEST')  #busco el flujo creado

        #imprimo la lista completa de Flujos despues de agregar el nuevo Flujo
        print Flujo.objects.all()
        print 'Test de Creacion de FLUJO ejecutado exitosamente.\n'

    def test_modificarFlujo(self):

        self.user = User.objects.get(pk=1)      #establezco el usuario administrador

        #imprimo todos los Flujos existentes antes de la modificacion
        #print Flujo.objects.all()

        flujo = Flujo.objects.get(id='1')       #establezo el flujo a modificar

        #id_flujo = '1'          #establezo el id del flujo a modificar
        id_proyecto = '1'       #establezo el id del Proyecto al cual pertenece el Flujo

        print 'Flujo Existente: '
        print '\tID: ' + str(flujo.id)
        print '\tNombre: ' + flujo.nombre

        request = self.factory.post('/adm_proyectos/1/adm_flujos/modificar/1/', {'Nombre_Flujo': 'MOD_flujo_MOD'})
        request.user = self.user
        response = modificarFlujo(request, flujo.id, id_proyecto)
        self.assertEqual(response.status_code, 200)

        #vuelvo a imprimir todos los flujos para ver el cambio
        #print Flujo.objects.all()

        flujo_mod = Flujo.objects.get(id='1')

        print 'Flujo Modificado: '
        print '\tID: ' + str(flujo_mod.id)
        print '\tNombre: ' + flujo_mod.nombre
        print 'Test de Modificacion de FLUJO ejecutado exitosamente.\n'

    def test_verFlujo(self):

        self.user = User.objects.get(pk=1)      #establezco el usuario administrador

        flujo = Flujo.objects.get(id='1')       #arbitrariamente selecciono un flujo

        id_proyecto = '1'       #establezo el id del Proyecto al cual pertenece el Flujo

        request = self.factory.post('/adm_proyectos/1/adm_flujos/ver/')
        request.user = self.user
        response = verFlujo(request, flujo.id, id_proyecto)
        self.assertEqual(response.status_code, 200)

        flujo_mod = Flujo.objects.get(id='1')

        print 'Ver Datos de Flujo: '
        print '\tID: ' + str(flujo_mod.id)
        print '\tNombre: ' + flujo_mod.nombre
        print 'Test de Ver FLUJO ejecutado exitosamente.'

    def test_eliminarFlujo(self):
        """ Cargamos en objeto request con el requerimiento de la url que nos
            permite eliminar un flujo.
        """

        #directamente voy a eliminar un flujo del proyecto con id=1
        request = self.factory.get('/adm_proyectos/1/adm_flujos/eliminar/')
        proyecto = Proyecto.objects.get(pk='1')

        #Asignamos a la variable user el usuario administrador.
        self.user = User.objects.get(pk=1)

        #Logueamos al usuario mediante el request
        request.user = self.user

        #Nos aseguramos que el flujo con el id existe.
        id = '1'
        flujo = Flujo.objects.get(pk=id)
        #print 'Flujo a Eliminar: ' + flujo.nombreFlujo
        print Flujo.objects.all()
        #self.assertFalse(flujo.borrado_logico)

        #Borramos el flujo con id 2
        response = eliminarFlujo(request, id, proyecto.id)

        #Nos aseguramos que el flujo con id se ha eliminado
        flujo = Flujo.objects.get(pk=id)
        self.assertTrue(flujo.borrado_logico)
        print Flujo.objects.filter(borrado_logico='FALSE')

        print 'Test de Eliminacion de Flujo ejecutado exitosamente.\n'

    def test_administrarActividades(self):

        self.user = User.objects.get(pk=1)              #Elijo el usuario Administrador

        #Solicito en el request ir a las Actividades de un Flujo
        request = self.factory.get('/adm_proyectos/1/adm_flujos/2/adm_actividades/')
        request.user = self.user
        proyecto = Proyecto.objects.get(id=1)           #elijo el proyecto con id=1
        self.assertTrue(proyecto)                       #verifico que exista el proyecto
        flujo = Flujo.objects.get(id=1)                 #elijo el flujo con id=2
        self.assertTrue(flujo)                          #verifico que exista el flujo

        #Pasamos a la pagina de administracion de Actividades del Flujo
        response = administrarActividades(request, flujo.id, proyecto.id)
        self.assertEqual(response.status_code, 200)
        print '\nTest de Administracion de Actividades ejecutado exitosamente.\n'

    def test_crearActividad(self):

        #imprimo la lista completa de Actividades antes de agregar la nueva
        print Actividad.objects.all()

        # Crea una instancia de una solicitud GET
        self.user = User.objects.get(pk=1)

        proyecto = Proyecto.objects.get(id=1)           #elijo el proyecto con id=1
        self.assertTrue(proyecto)                       #verifico que exista el proyecto
        flujo = Flujo.objects.get(id=1)                 #elijo el flujo con id=2
        self.assertTrue(flujo)                          #verifico que exista el flujo

        #completamos en el request el formulario para una nueva Actividad en el Flujo con id=2
        request = self.factory.post('adm_proyectos/1/adm_flujos/2/adm_actividades/nuevo/', {'Orden_Actividad': '3', 'Nombre_Actividad': 'ActividadNueva'})
        request.user = self.user
        response = crearActividad(request, flujo.id, proyecto.id)   #mando a la vista de creacion de nueva Actividad
        self.assertEqual(response.status_code, 200)                 #verifico que se haya recibido la pagina
        #busco = Flujo.objects.get(nombreFlujo='FlujoTEST')         #busco la Actividad Creada

        #imprimo la lista completa de Actividades despues de agregar
        print Actividad.objects.all()
        print 'Test de Creacion de Actividad ejecutado exitosamente.\n'

    """
        def test_modificarActividad(self):

        self.user = User.objects.get(pk=1)      #establezco el usuario administrador

        actividad = Actividad.objects.get(id=1)         #establezo la actividad a modificar
        flujo = Flujo.objects.get(id=2)                 #establezo el flujo
        proyecto = Proyecto.objects.get(id=1)           #establezo el proyecto

        print 'Actividad Existente: '
        print '\tID: ' + str(actividad.id)
        print '\tNombre: ' + actividad.nombreActividad


        request = self.factory.post('/adm_proyectos/1/adm_flujos/2/adm_actividades/modificar/', {'nombreActividad': 'MOD_flujo_MOD', 'flujoCorrespondiente': 'MOD_flujo_MOD', 'ordenActividad': 'MOD_flujo_MOD'})
        request.user = self.user
        response = modificarFlujo(request, flujo.id, id_proyecto)
        self.assertEqual(response.status_code, 200)

        #vuelvo a imprimir todos los flujos para ver el cambio
        #print Flujo.objects.all()

        flujo_mod = Flujo.objects.get(id='1')

        print 'Flujo Modificado: '
        print '\tID: ' + str(flujo_mod.id)
        print '\tNombre: ' + flujo_mod.nombreFlujo
        print 'Test de Modificacion de FLUJO ejecutado exitosamente.\n'
    """