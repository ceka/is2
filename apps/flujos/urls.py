from django.views.generic import TemplateView

__author__ = 'eduardo'
from django.conf.urls import patterns, url
from .views import crearFlujo, eliminarFlujo
from .views import modificarFlujo, verFlujo, administrarFlujos, confEliminarFlujo
from .views import administrarActividades, crearActividad, modificarActividad, verActividad, eliminarActividad

urlpatterns = patterns('',
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/$', administrarFlujos),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/nuevo/$', crearFlujo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/modificar/(?P<id_flujo>\d+)/$', modificarFlujo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/ver/(?P<id_flujo>\d+)/$', verFlujo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/eliminar/(?P<id_flujo>.*)/$', eliminarFlujo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/confirmar_eliminar/(?P<id_flujo>.*)/$', confEliminarFlujo),


        #urls de actividades por flujo
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/adm_actividades/$', administrarActividades),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/adm_actividades/nuevo/$', crearActividad),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/adm_actividades/modificar/(?P<id_actividad>\d+)/$', modificarActividad),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/adm_actividades/ver/(?P<id_actividad>\d+)/$', verActividad),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/adm_actividades/eliminar/(?P<id_actividad>\d+)/$', eliminarActividad),

        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_flujos/(?P<id_flujo>\d+)/denegado', TemplateView.as_view(template_name='403_flujos.html')),
)
