from django.contrib.auth.models import User, Group
from django.db import models
from apps.cliente.models import Cliente

__author__ = 'hdmedina'
ESTADOS = (

    ('PEN', 'Pendiente'),
    ('CAN','Cancelado'),
    ('ACT', 'Activo'),
    ('FIN','Finalizado')
)

class Proyecto(models.Model):
    """
    Model de Proyecto con atributos correspondientes a la base de datos.
    """
    nombre= models.CharField(max_length=100, verbose_name='Nombre',unique=True)
    descripcion= models.TextField(verbose_name='Descripcion')
    fecha_ini=models.DateField(null=False)
    fecha_fin=models.DateField(null=False)
    estado=models.CharField(max_length=3,choices= ESTADOS, default='PEN')
    duracion_sprint = models.IntegerField()
    lider = models.ForeignKey(User, related_name='lider')
    observaciones = models.TextField(verbose_name='Observaciones')
    fecha_fin_real=models.DateField(null=True)
    cliente = models.ForeignKey(Cliente, related_name='cliente')
    borrado_logico = models.BooleanField(default=False)
    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name = 'proyecto'
        verbose_name_plural = 'proyectos'
        permissions = (
            ('listar_proyecto', 'Puede listar proyecto'),
            ('crear_proyecto', 'Puede crear proyecto'),
            ('cambiar_estado_proyecto', 'Puede cambiar estado del proyecto'),
            ('eliminar_proyecto', 'Puede eliminar proyecto'),
            ('modificar_detalle_proyecto', 'Puede modificar detalle de proyecto'),
            ('abrir_proyecto', 'Puede abrir proyecto'),
        )

class Equipo(models.Model):
    """
    Tabla que sirve para saber el rol que le corresponde a un usuario dentro del cada proyecto
    """
    miembro = models.ForeignKey(User, related_name='miembro')
    rol = models.ForeignKey(Group, related_name='rol')
    proyecto = models.ForeignKey(Proyecto, related_name='proyecto')

    def __unicode__(self):
        return self.miembro.username

    class Meta:
        verbose_name = 'equipo'
        verbose_name_plural = 'equipos'
        permissions = (
            ('listar_equipo', 'Puede listar equipo'),
            ('anhadir_miembro', 'Puede anhadir miembro'),
            ('quitar_miembro', 'Puede quitar miembro'),
        )