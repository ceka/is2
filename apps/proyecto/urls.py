from django.conf.urls import patterns, url
from django.contrib import admin
from apps.proyecto.views import listar_proyectos,abrir_proyecto, crear_proyecto, register_success, detalle_proyecto, buscar_proyectos, editar_proyecto, \
    eliminacion_logica, cambiar_estado, administrarEquipo ,agregarMiembro, confQuitarMiembro, quitarMiembro

__author__ = 'hdmedina'

admin.autodiscover()

urlpatterns = patterns('',
        url(r'^adm_proyectos/$',listar_proyectos),
        url(r'^adm_proyectos/nuevo/$', crear_proyecto),
        url(r'^adm_proyectos/register/success',register_success),
        url(r'^adm_proyectos/abrir_proyecto/(?P<id_proyecto>\d+)/$',abrir_proyecto),
        url(r'^adm_proyectos/ver/(?P<id_proyecto>\d+)/$', detalle_proyecto),
        url(r'^adm_proyectos/buscar',buscar_proyectos, name='buscar_proyectos'),
        url(r'^adm_proyectos/editar_proyecto/(?P<id_proyecto>\d+)$', editar_proyecto),
        url(r'^adm_proyectos/eliminar_proyecto/(?P<id_proyecto>\d+)$', eliminacion_logica),
        url(r'^adm_proyectos/cambiar_estado/(?P<id_proyecto>\d+)/$',cambiar_estado),

        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_equipo/$',administrarEquipo),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_equipo/nuevo/$',agregarMiembro),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_equipo/eliminar/(?P<id_equipo>.*)/$', quitarMiembro),
        url(r'^adm_proyectos/(?P<id_proyecto>\d+)/adm_equipo/confirmar_eliminar/(?P<id_equipo>.*)/$', confQuitarMiembro),


)
