from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.auth.models import User, Group
from django.utils.datetime_safe import datetime
from apps.cliente.models import Cliente
from apps.proyecto.models import Proyecto

__author__ = 'hdmedina'

ESTADOS = (

    ('PEN', 'Pendiente'),
    ('CAN','Cancelado'),
    ('ACT', 'Activo'),
    ('FIN', 'Finalizado'),
)
today = datetime.now() #fecha actual
dateFormat = today.strftime("%Y-%m-%d") # fecha con format


class ProyectoForm(forms.ModelForm):
        nombre= forms.CharField(max_length=100)
        descripcion= forms.CharField(label='Descripcion', widget=forms.Textarea)
        fecha_ini=forms.DateField(input_formats=['%Y-%m-%d'], widget = AdminDateWidget, label='Fecha de Inicio',initial=dateFormat)
        #fecha_fin=forms.DateField(input_formats=['%Y-%m-%d'],widget = AdminDateWidget, label='Fecha de finalizacion')
        fecha_fin=forms.DateField(widget=forms.TextInput(attrs={'placeholder':'AAAA-MM-DD'}), label='Fecha de finalizacion')
        duracion_sprint = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'En semanas'}), label='Duracion de los Sprints')
        lider = forms.ModelChoiceField(queryset=User.objects.filter(is_active=True, is_superuser=False))
        cliente = forms.ModelChoiceField(queryset=Cliente.objects.all())
        observaciones = forms.CharField(label='Observaciones', widget=forms.Textarea)
        class Meta:
            model = Proyecto
            exclude = ['estado', 'fecha_fin_real','borrado_logico']

class CambiarEstadoForm(forms.ModelForm):
    estado=forms.CharField(max_length=3,widget=forms.Select(choices= ESTADOS))
    class Meta:
        model = Proyecto
        exclude = ['nombre','duracion_sprint', 'descripcion', 'fecha_ini', 'fecha_fin', 'lider','cliente', 'observaciones', 'fecha_fin_real','borrado_logico']

class MiembroNuevoForm (forms.Form):
    """

    :type: request: django.http.HttpRequest
    :param: request: Contiene informacion sobre la solic. web actual que llamo a esta vista

    :rtype: django.http.HttpResponse
    :return: ninguna

    :author: Eduardo Mendez
    """

    Usuario = forms.ModelChoiceField(queryset=User.objects.all(), required=True, error_messages={'required': 'Seleccione un Usuario', })
    Rol = forms.ModelChoiceField(queryset=Group.objects.exclude(name = 'Lider'), required=True, error_messages={'required': 'Seleccione un Rol', })
