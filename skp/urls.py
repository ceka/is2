
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sap.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

     url(r'^admin/', include(admin.site.urls)),

     #""" Incluimos la urls.py de la aplicacion Autenticacion """
    url(r'^', include('apps.autenticacion.urls')),
    #""" Incluimos la urls.py de la aplicacion usuario """
    url(r'^', include('apps.usuario.urls')),
    #""" Incluimos la urls.py de la aplicacion roles """
    url(r'^roles/', include('apps.roles.urls')),
        #""" Incluimos la urls.py de la aplicacion cliente """
    url(r'^', include('apps.cliente.urls')),

        #""" Incluimos la urls.py de la aplicacion flujo """
    url(r'^', include('apps.flujos.urls')),

    #""" Incluimos la urls.py de la aplicacion proyecto """
    url(r'^', include('apps.proyecto.urls')),

    #""" Incluimos la urls.py de la aplicacion proyecto """
    url(r'^', include('apps.user_stories.urls')),

        #""" Incluimos la urls.py de la aplicacion sprint """
    url(r'^', include('apps.sprint.urls')),

    #""" Pagina de acceso denegado """
    url(r'^denegado/$',TemplateView.as_view(template_name='403.html')),

)
