#!/bin/bash

chown -R www-data /var/www/html
if [ "$?" -ne 0 ]
then
    echo -e "ERROR: No se pudo cambiar el duenho del directorio /var/www/html/skp"
    exit 1
fi
chgrp -R www-data /var/www/html
if [ "$?" -ne 0 ]
then
    echo -e "ERROR: No se pudo cambiar el grupo del directorio /var/www/html/skp"
    exit 1
fi
echo -e "Archivos copiados\n"

# TODO: Eliminar al desinstalar la aplicacion
echo -e "----Configurando Apache----"
mv /var/www/html/skp/conf/*.conf /etc/apache2/sites-available/
if [ "$?" -ne 0 ]
then
    echo -e "ERROR: No se mover los archivos de configuracion desde /var/www/zarPm/conf/ a /etc/apache2/sites-available"
    exit 1
fi

echo -e "Activando los sitios [skp] en Apache"
a2ensite skp.conf
if [ "$?" -ne 0 ]
then
    echo -e "ERROR: No se pudo activar el sitio skp.conf"
    exit 1
fi

echo -e "Recargando Apache"
service apache2 reload
if [ "$?" -ne 0 ]
then
    echo -e "ERROR: No se pudo recargar el servicio apache2"
    exit 1
fi

# TODO: Verificar si estos datos en /etc/hosts antes de agregarlos, actualmente se agregan cada vez que se ejecuta el archivo
# TODO: Eliminar al desinstalar la aplicacion
echo -e "----Fix[sin DNS]: Agrega el nombre y direccion de la pagina a los hosts conocidos de la maquina.----"
echo "127.0.0.1 skp.com" >> /etc/hosts
#echo "127.0.0.1 static-zarpm.org" >> /etc/hosts

echo "----Fin----"
exit 0
